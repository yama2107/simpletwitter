package chapter6.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import chapter6.beans.Message;
import chapter6.service.MessageService;

@WebServlet(urlPatterns = {"/edit"})
public class EditServlet extends HttpServlet {

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
		throws IOException, ServletException {
		HttpSession session = request.getSession();
		List<String> errorMessages = new ArrayList<String>();

		String messageId = request.getParameter("messageId");

		if(!isValidUrl(messageId, errorMessages)) {
			session.setAttribute("errorMessages", errorMessages);
			response.sendRedirect("./");
			return;
		}

		int id = Integer.parseInt(messageId);

		Message message = new MessageService().editselect(id);

		if(message ==  null) {
			session.setAttribute("errorMessages", "不正なパラメータが入力されました");
			response.sendRedirect("./");
			return;
		}

		request.setAttribute("message", message);
		request.getRequestDispatcher("edit.jsp").forward(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
		throws IOException, ServletException {

		List<String> errorMessages = new ArrayList<String>();

        int id = Integer.parseInt(request.getParameter("messageId"));
        String text = request.getParameter("text");

        if(!isValid(text, errorMessages)) {
        	Message message = new MessageService().editselect(id);
        	request.setAttribute("errorMessages", errorMessages);
        	request.setAttribute("message", message);
        	request.getRequestDispatcher("edit.jsp").forward(request, response);
        }
        new MessageService().update(id, text);
        response.sendRedirect("./");
	}

	private boolean isValid(String text, List<String> errorMessages) {

		if(StringUtils.isBlank(text)) {
			errorMessages.add("入力してください");
		} else if(140 < text.length()) {
			errorMessages.add("140文字以下で入力してください");
		}

		if(errorMessages.size() != 0) {
			return false;
		}
		return true;
	}

	private boolean isValidUrl(String messageId, List<String> errorMessages) {
		if(StringUtils.isBlank(messageId)) {
			errorMessages.add("不正なパラメータが入力されました");
		} else if(!messageId.matches("^[0-9]*$")) {
			errorMessages.add("不正なパラメータが入力されました");
		}

		if(errorMessages.size() != 0) {
			return false;
		}
		return true;
	}

}
