package chapter6.dao;

import static chapter6.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import chapter6.beans.Message;
import chapter6.exception.SQLRuntimeException;

public class MessageDao {

	public void insert(Connection connection, Message message) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
            sql.append("INSERT INTO messages ( ");
            sql.append("    user_id, ");
            sql.append("    text, ");
            sql.append("    created_date, ");
            sql.append("    updated_date ");
            sql.append(") VALUES ( ");
            sql.append("    ?, ");                                  // user_id
            sql.append("    ?, ");                                  // text
            sql.append("    CURRENT_TIMESTAMP, ");  // created_date
            sql.append("    CURRENT_TIMESTAMP ");       // updated_date
            sql.append(")");

            ps = connection.prepareStatement(sql.toString());

            ps.setInt(1, message.getUserId());
            ps.setString(2, message.getText());

            ps.executeUpdate();
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	public Message editselect(Connection connection, int id) {

		PreparedStatement ps = null;
		try {
		  	StringBuilder sql = new StringBuilder();
		  	sql.append("SELECT * FROM messages ");
		  	sql.append("WHERE id = ? ");

		  	ps = connection.prepareStatement(sql.toString());
		  	ps.setInt(1, id);

            ResultSet rs = ps.executeQuery();

            List<Message> messages = toMessage(rs);

            if (messages.isEmpty()) {
                return null;
            } else {
                return messages.get(0);
            }
		} catch(SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	  }

	public void update(Connection connection, int id, String text) {
		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("UPDATE messages ");
			sql.append("SET text = ? ");
			sql.append("WHERE id = ? ");

			ps = connection.prepareStatement(sql.toString());

			ps.setString(1, text);
			ps.setInt(2, id);

			ps.executeUpdate();
		} catch(SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	public void delete(Connection connection, int messageId) {

		PreparedStatement ps = null;
		try {
		  	StringBuilder sql = new StringBuilder();
		  	sql.append("DELETE FROM messages ");
		  	sql.append("WHERE id = ? ");

		  	ps = connection.prepareStatement(sql.toString());
		  	ps.setInt(1, messageId);
		  	ps.executeUpdate();
		} catch(SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	private List<Message> toMessage(ResultSet rs) throws SQLException {
		List<Message> messages = new ArrayList<Message>();
		try {
			while(rs.next()) {
				Message message = new Message();
				message.setId(rs.getInt("id"));
				message.setUserId(rs.getInt("user_id"));
				message.setText(rs.getString("text"));
				message.setCreatedDate(rs.getTimestamp("created_date"));
				message.setUpdatedDate(rs.getTimestamp("updated_date"));

				messages.add(message);
			}
			return messages;
		} finally {
			close(rs);
		}
	}
}
